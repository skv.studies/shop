package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Employee;
import com.neoflex.study.shop.controller.exception.ObjectNotFoundException;
import com.neoflex.study.shop.repository.EmployeeRepository;
import com.neoflex.study.shop.service.impl.EmployeeServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {
    @InjectMocks
    private EmployeeServiceImpl employeeServiceImpl;

    @Mock
    private EmployeeRepository employeeRepository;

    private Employee employee;

    @Before
    public void init() {
        employee = new Employee();
    }

    @Test
    public void findById() {
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        Assert.assertEquals(employeeServiceImpl.findById(employee.getId()), employee);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void findByIdObjectNotFoundException() {
        when(employeeRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Employee.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        employeeServiceImpl.findById("0a742fb4-6523-11eb-ae93-0242ac130002");
    }

    @Test
    public void findAll() {
        List<Employee> customers = Arrays.asList(employee);
        when(employeeRepository.findAll()).thenReturn(customers);
        Assert.assertEquals(employeeServiceImpl.findAll(), customers);
    }

    @Test
    public void create() {
        Employee employee = new Employee();
        when(employeeRepository.saveAndFlush(employee)).thenReturn(employee);
        Assert.assertEquals(employeeServiceImpl.create(employee), employee.getId());
    }

    @Test
    public void update() {
        Employee newEmployee = new Employee();
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        (lenient()).when(employeeRepository.saveAndFlush(employee)).thenReturn(null);
        employeeServiceImpl.update(employee.getId(), newEmployee);
        verify(employeeRepository, times(1)).saveAndFlush(newEmployee);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void updateEmployeeNotFoundException() {
        when(employeeRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Employee.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        employeeServiceImpl.update("0a742fb4-6523-11eb-ae93-0242ac130002", employee);
    }

    @Test
    public void delete() {
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        doNothing().when(employeeRepository).deleteById(employee.getId());
        employeeServiceImpl.delete(employee.getId());
        verify(employeeRepository, times(1)).deleteById(employee.getId());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void deleteEmployeeNotFoundException() {
        when(employeeRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Employee.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        employeeServiceImpl.delete("0a742fb4-6523-11eb-ae93-0242ac130002");
    }
}