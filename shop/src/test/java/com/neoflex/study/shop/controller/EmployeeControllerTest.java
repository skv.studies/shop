package com.neoflex.study.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoflex.study.shop.dto.*;
import com.neoflex.study.shop.entity.Employee;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.mapper.EmployeeInputMapper;
import com.neoflex.study.shop.mapper.EmployeeOutputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private EmployeeInputMapper employeeInputMapper;

    @MockBean
    private EmployeeOutputMapper employeeOutputMapper;

    @MockBean
    private OrderMapper orderMapper;

    private Employee employee;
    private EmployeeInputDTO employeeInputDTO;
    private EmployeeOutputDTO employeeOutputDTO;

    @Before
    public void init() {
        employee = new Employee("Ivan", "packer", Arrays.asList(new Order()));
        employeeInputDTO = new EmployeeInputDTO(employee.getFullName(), employee.getPosition());
        employeeOutputDTO = new EmployeeOutputDTO(employee.getFullName(), employee.getPosition(),
                Arrays.asList(new OrderOutputDTO()));
    }

    @Test
    public void getEmployee() throws Exception {
        given(employeeService.findById(employee.getId())).willReturn(employee);
        when(employeeOutputMapper.employeeToEmployeeOutputDTO(employee)).thenReturn(employeeOutputDTO);
        when(orderMapper.orderToOrderDTO(new Order())).thenReturn(new OrderOutputDTO());
        mvc.perform(get("/api/v2/employees/{id}", employee.getId())
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName").value(employeeOutputDTO.getFullName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.position").value(employeeOutputDTO.getPosition()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ordersDTO").value(employeeOutputDTO.getOrdersDTO()));
    }

    @Test
    public void getAllEmployees() throws Exception {
        List<Employee> employees = Arrays.asList(employee);
        given(employeeService.findAll()).willReturn(employees);
        when(employeeOutputMapper.employeeToEmployeeOutputDTO(employee)).thenReturn(employeeOutputDTO);
        when(orderMapper.orderToOrderDTO(new Order())).thenReturn(new OrderOutputDTO());
        mvc.perform(get("/api/v2/employees")
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createEmployee() throws Exception {
        given(employeeService.create(employee)).willReturn(employee.getId());
        when(employeeInputMapper.employeeInputDTOToEmployee(any(EmployeeInputDTO.class))).thenReturn(employee);
        mvc.perform(post("/api/v2/employees")
                .content(new ObjectMapper().writeValueAsString(employeeInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isCreated())
                .andExpect(content().string(employee.getId()));
    }

    @Test
    public void updateEmployee() throws Exception {
        when(employeeInputMapper.employeeInputDTOToEmployee(employeeInputDTO)).thenReturn(employee);
        doNothing().when(employeeService).update("id", employee);
        mvc.perform(put("/api/v2/employees/{id}", "id")
                .content(new ObjectMapper().writeValueAsString(employeeInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteEmployee() throws Exception {
        doNothing().when(employeeService).delete(employee.getId());
        mvc.perform(delete("/api/v2/employees/{id}", employee.getId())
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }
}