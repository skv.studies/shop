package com.neoflex.study.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoflex.study.shop.dto.ProductDTO;
import com.neoflex.study.shop.entity.Product;
import com.neoflex.study.shop.mapper.ProductMapper;
import com.neoflex.study.shop.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductMapper productMapper;

    Product product;
    ProductDTO productDTO;

    @Before
    public void init() {
        product = new Product("electronics", "tv", "tv", 20.0f);
        productDTO = new ProductDTO(product.getType(), product.getName(), product.getDescription(), product.getPrice());
    }

    @Test
    public void getProduct() throws Exception {
        given(productService.findById(product.getId())).willReturn(product);
        when(productMapper.productToProductDTO(product)).thenReturn(productDTO);
        mvc.perform(get("/api/v2/products/{id}", product.getId())
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").value(productDTO.getType()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(productDTO.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(productDTO.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(productDTO.getPrice()));
    }

    @Test
    public void getAllProducts() throws Exception {
        List<Product> products = Arrays.asList(product);
        given(productService.findAll()).willReturn(products);
        when(productMapper.productToProductDTO(product)).thenReturn(productDTO);
        mvc.perform(get("/api/v2/products")
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createProduct() throws Exception {
        given(productService.create(product)).willReturn(product.getId());
        when(productMapper.productDTOToProduct(any(ProductDTO.class))).thenReturn(product);
        mvc.perform(post("/api/v2/products")
                .content(new ObjectMapper().writeValueAsString(productDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isCreated())
                .andExpect(content().string(product.getId()));
    }

    @Test
    public void updateProduct() throws Exception {
        doNothing().when(productService).update("id", product);
        when(productMapper.productDTOToProduct(any(ProductDTO.class))).thenReturn(product);
        mvc.perform(put("/api/v2/products/{id}", "id")
                .content(new ObjectMapper().writeValueAsString(productDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteProduct() throws Exception {
        doNothing().when(productService).delete(product.getId());
        mvc.perform(delete("/api/v2/products/{id}", product.getId())
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }
}