package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Product;
import com.neoflex.study.shop.controller.exception.ObjectNotFoundException;
import com.neoflex.study.shop.repository.ProductRepository;
import com.neoflex.study.shop.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {
    @InjectMocks
    private ProductServiceImpl productServiceImpl;

    @Mock
    private ProductRepository productRepository;

    private Product product;

    @Before
    public void init() {
        product = new Product();
    }

    @Test
    public void findById() {
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        Assert.assertEquals(productServiceImpl.findById(product.getId()), product);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void findByIdObjectNotFoundException() {
        when(productRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Product.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        productServiceImpl.findById("0a742fb4-6523-11eb-ae93-0242ac130002");
    }

    @Test
    public void findAll() {
        List<Product> products = Arrays.asList(product);
        when(productRepository.findAll()).thenReturn(products);
        Assert.assertEquals(productServiceImpl.findAll(), products);
    }

    @Test
    public void create() {
        when(productRepository.saveAndFlush(product)).thenReturn(product);
        Assert.assertEquals(productServiceImpl.create(product), product.getId());
    }

    @Test
    public void update() {
        Product newProduct = new Product();
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        (lenient()).when(productRepository.saveAndFlush(product)).thenReturn(null);
        productServiceImpl.update(product.getId(), newProduct);
        verify(productRepository, times(1)).saveAndFlush(newProduct);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void updateProductNotFoundException() {
        when(productRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Product.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        productServiceImpl.update("0a742fb4-6523-11eb-ae93-0242ac130002", product);
    }

    @Test
    public void delete() {
        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        doNothing().when(productRepository).deleteById(product.getId());
        productServiceImpl.delete(product.getId());
        verify(productRepository, times(1)).deleteById(product.getId());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void deleteProductNotFoundException() {
        when(productRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Product.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        productServiceImpl.delete("0a742fb4-6523-11eb-ae93-0242ac130002");
    }
}