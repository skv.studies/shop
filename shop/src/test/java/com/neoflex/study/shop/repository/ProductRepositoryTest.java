package com.neoflex.study.shop.repository;

import com.neoflex.study.shop.ShopApplication;
import com.neoflex.study.shop.entity.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {ShopApplication.class})
public class ProductRepositoryTest {
    @Autowired
    private ProductRepository productRepository;

    @Test
    @Transactional
    public void sort() {
        List<Product> products = productRepository.findAll();
        products.sort(Comparator.comparing(Product::getPrice));
        Assert.assertEquals(productRepository.sort(Sort.by("price")), products);
    }

    @Test
    @Transactional
    public void updatePrice() {
        String id = "134a364c-1db6-11eb-adc1-0242ac120002";
        float price = 40.0f;
        productRepository.updatePrice(id, price);
        Assert.assertEquals(productRepository
                .findById(id)
                .orElse(new Product())
                .getPrice(), price, 0);
    }
}