package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.controller.exception.ObjectNotFoundException;
import com.neoflex.study.shop.repository.OrderRepository;
import com.neoflex.study.shop.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    @InjectMocks
    private OrderServiceImpl orderServiceImpl;

    @Mock
    private OrderRepository orderRepository;

    private Order order;

    @Before
    public void init() {
        order = new Order();
    }

    @Test
    public void findById() {
        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
        Assert.assertEquals(orderServiceImpl.findById(order.getId()), order);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void findByIdObjectNotFoundException() {
        when(orderRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Order.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        orderServiceImpl.findById("0a742fb4-6523-11eb-ae93-0242ac130002");
    }

    @Test
    public void findAll() {
        List<Order> orders = Arrays.asList(order);
        when(orderRepository.findAll()).thenReturn(orders);
        Assert.assertEquals(orderServiceImpl.findAll(), orders);
    }

    @Test
    public void create() {
        when(orderRepository.saveAndFlush(order)).thenReturn(order);
        Assert.assertEquals(orderServiceImpl.create(order), order.getId());
    }

    @Test
    public void update() {
        Order newOrder = new Order();
        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
        (lenient()).when(orderRepository.saveAndFlush(order)).thenReturn(null);
        orderServiceImpl.update(order.getId(), newOrder);
        verify(orderRepository, times(1)).saveAndFlush(newOrder);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void updateOrderNotFoundException() {
        when(orderRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Order.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        orderServiceImpl.update("0a742fb4-6523-11eb-ae93-0242ac130002", order);
    }

    @Test
    public void delete() {
        when(orderRepository.findById(order.getId())).thenReturn(Optional.of(order));
        doNothing().when(orderRepository).deleteById(order.getId());
        orderServiceImpl.delete(order.getId());
        verify(orderRepository, times(1)).deleteById(order.getId());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void deleteOrderNotFoundException() {
        when(orderRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Order.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        orderServiceImpl.delete("0a742fb4-6523-11eb-ae93-0242ac130002");
    }
}