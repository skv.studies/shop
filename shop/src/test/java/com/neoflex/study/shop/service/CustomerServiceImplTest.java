package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Customer;
import com.neoflex.study.shop.controller.exception.ObjectNotFoundException;
import com.neoflex.study.shop.repository.CustomerRepository;
import com.neoflex.study.shop.service.impl.CustomerServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {
    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @Mock
    private CustomerRepository customerRepository;

    private Customer customer;

    @Before
    public void init() {
        customer = new Customer();
    }

    @Test
    public void findById() {
        when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
        Assert.assertEquals(customerServiceImpl.findById(customer.getId()), customer);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void findByIdObjectNotFoundException() {
        when(customerRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Customer.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        customerServiceImpl.findById("0a742fb4-6523-11eb-ae93-0242ac130002");
    }

    @Test
    public void findAll() {
        List<Customer> customers = Arrays.asList(customer);
        when(customerRepository.findAll()).thenReturn(customers);
        Assert.assertEquals(customerServiceImpl.findAll(), customers);
    }

    @Test
    public void create() {
        when(customerRepository.saveAndFlush(customer)).thenReturn(customer);
        Assert.assertEquals(customerServiceImpl.create(customer), customer.getId());
    }

    @Test
    public void update() {
        Customer newCustomer = new Customer();
        when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
        (lenient()).when(customerRepository.saveAndFlush(customer)).thenReturn(null);
        customerServiceImpl.update(customer.getId(), newCustomer);
        verify(customerRepository, times(1)).saveAndFlush(newCustomer);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void updateCustomerNotFoundException() {
        when(customerRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Customer.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        customerServiceImpl.update("0a742fb4-6523-11eb-ae93-0242ac130002", customer);
    }

    @Test
    public void delete() {
        when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(customer));
        doNothing().when(customerRepository).deleteById(customer.getId());
        customerServiceImpl.delete(customer.getId());
        verify(customerRepository, times(1)).deleteById(customer.getId());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void deleteCustomerNotFoundException() {
        when(customerRepository.findById("0a742fb4-6523-11eb-ae93-0242ac130002"))
                .thenThrow(new ObjectNotFoundException(Customer.class.getSimpleName(), "0a742fb4-6523-11eb-ae93-0242ac130002"));
        customerServiceImpl.delete("0a742fb4-6523-11eb-ae93-0242ac130002");
    }
}