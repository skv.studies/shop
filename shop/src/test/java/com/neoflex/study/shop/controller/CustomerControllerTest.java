package com.neoflex.study.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.neoflex.study.shop.dto.CustomerInputDTO;
import com.neoflex.study.shop.dto.CustomerOutputDTO;
import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.entity.Customer;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.mapper.CustomerInputMapper;
import com.neoflex.study.shop.mapper.CustomerOutputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.serializer.CustomerInputDTOSerializer;
import com.neoflex.study.shop.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private CustomerInputMapper customerInputMapper;

    @MockBean
    private CustomerOutputMapper customerOutputMapper;

    @MockBean
    private OrderMapper orderMapper;

    private Customer customer;
    private CustomerInputDTO customerInputDTO;
    private CustomerOutputDTO customerOutputDTO;

    @Before
    public void init() {
        customer = new Customer(new Date(), "male", "Ivan", "89111111111", "email@mail.ru",
                "address", Arrays.asList(new Order()));
        customerInputDTO = new CustomerInputDTO(customer.getBirthday(), customer.getGender(),
                customer.getFullName(), customer.getPhone(), customer.getEmail(), customer.getAddress());
        customerOutputDTO = new CustomerOutputDTO(customer.getBirthday(), customer.getGender(),
                customer.getFullName(), customer.getPhone(), customer.getEmail(), customer.getAddress(),
                Arrays.asList(new OrderOutputDTO()));
    }

    @Test
    public void getCustomer() throws Exception {
        given(customerService.findById(customer.getId())).willReturn(customer);
        when(customerOutputMapper.customerToCustomerOutputDTO(customer)).thenReturn(customerOutputDTO);
        when(orderMapper.orderToOrderDTO(new Order())).thenReturn(new OrderOutputDTO());
        mvc.perform(get("/api/v2/customers/{id}", customer.getId())
                .header("login", "user")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthday").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(customerOutputDTO.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName").value(customerOutputDTO.getFullName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phone").value(customerOutputDTO.getPhone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(customerOutputDTO.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address").value(customerOutputDTO.getAddress()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ordersDTO").value(customerOutputDTO.getOrdersDTO()));
    }

    @Test
    public void getAllCustomers() throws Exception {
        List<Customer> customers = Arrays.asList(customer);
        given(customerService.findAll()).willReturn(customers);
        when(customerOutputMapper.customerToCustomerOutputDTO(customer)).thenReturn(customerOutputDTO);
        when(orderMapper.orderToOrderDTO(new Order())).thenReturn(new OrderOutputDTO());
        mvc.perform(get("/api/v2/customers")
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createCustomer() throws Exception {
        given(customerService.create(customer)).willReturn(customer.getId());
        when(customerInputMapper.customerInputDTOToCustomer(any(CustomerInputDTO.class))).thenReturn(customer);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(CustomerInputDTO.class, new CustomerInputDTOSerializer());
        mapper.registerModule(module);
        mvc.perform(post("/api/v2/customers")
                .content(mapper.writeValueAsString(customerInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isCreated())
                .andExpect(content().string(customer.getId()));
    }

    @Test
    public void updateCustomer() throws Exception {
        when(customerInputMapper.customerInputDTOToCustomer(any(CustomerInputDTO.class))).thenReturn(customer);
        doNothing().when(customerService).update("id", customer);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(CustomerInputDTO.class, new CustomerInputDTOSerializer());
        mapper.registerModule(module);
        mvc.perform(put("/api/v2/customers/{id}", "id")
                .content(mapper.writeValueAsString(customerInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteCustomer() throws Exception {
        doNothing().when(customerService).delete(customer.getId());
        mvc.perform(delete("/api/v2/customers/{id}", customer.getId())
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }
}