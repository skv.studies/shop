package com.neoflex.study.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.neoflex.study.shop.dto.OrderInputDTO;
import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.entity.Customer;
import com.neoflex.study.shop.entity.Employee;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.entity.Product;
import com.neoflex.study.shop.mapper.OrderInputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.serializer.OrderInputDTOSerializer;
import com.neoflex.study.shop.service.CustomerService;
import com.neoflex.study.shop.service.EmployeeService;
import com.neoflex.study.shop.service.OrderService;
import com.neoflex.study.shop.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderService orderService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private ProductService productService;

    @MockBean
    private OrderMapper orderMapper;

    @MockBean
    private OrderInputMapper orderInputMapper;

    private Product product;
    private Customer customer;
    private Employee employee;
    private Order order;
    private OrderInputDTO orderInputDTO;
    private OrderOutputDTO orderOutputDTO;

    @Before
    public void init() throws Exception {
        product = new Product();
        customer = new Customer(new Date(), "male", "Ivan", "89111111111", "email@mail.ru",
                "address", Arrays.asList(new Order()));
        employee = new Employee("Ivan", "packer", Arrays.asList(new Order()));
        order = new Order(new Date(), new Date(), "finished", customer, customer.getId(), employee,
                employee.getId(), Arrays.asList(product));
        orderInputDTO = new OrderInputDTO(order.getCreateDate(), order.getFinishDate(), order.getStatus(),
                order.getCustomerId(), order.getEmployeeId(), Arrays.asList(product.getId()));
        orderOutputDTO = new OrderOutputDTO(order.getCreateDate(), order.getFinishDate(), order.getStatus(),
                order.getCustomerId(), order.getEmployeeId(), Arrays.asList(product));

    }

    @Test
    public void getOrder() throws Exception {
        given(orderService.findById(order.getId())).willReturn(order);
        when(orderMapper.orderToOrderDTO(order)).thenReturn(orderOutputDTO);
        mvc.perform(get("/api/v2/orders/{id}", order.getId())
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.createDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finishDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(orderOutputDTO.getStatus()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId").value(orderOutputDTO.getCustomerId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeId").value(orderOutputDTO.getEmployeeId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.products").exists());
    }

    @Test
    public void getAllOrders() throws Exception {
        List<Order> orders = Arrays.asList(order);
        given(orderService.findAll()).willReturn(orders);
        when(orderMapper.orderToOrderDTO(order)).thenReturn(orderOutputDTO);
        mvc.perform(get("/api/v2/orders")
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createOrder() throws Exception {
        given(orderService.create(order)).willReturn(order.getId());
        when(orderInputMapper.orderInputDTOToOrder(any(OrderInputDTO.class))).thenReturn(order);
        when(customerService.findById(customer.getId())).thenReturn(customer);
        when(employeeService.findById(employee.getId())).thenReturn(employee);
        when(productService.findById(product.getId())).thenReturn(product);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(OrderInputDTO.class, new OrderInputDTOSerializer());
        mapper.registerModule(module);
        mvc.perform(post("/api/v2/orders")
                .content(mapper.writeValueAsString(orderInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isCreated())
                .andExpect(content().string(order.getId()));
    }

    @Test
    public void updateOrder() throws Exception {
        doNothing().when(orderService).update("id", order);
        when(orderInputMapper.orderInputDTOToOrder(any(OrderInputDTO.class))).thenReturn(order);
        when(customerService.findById(customer.getId())).thenReturn(customer);
        when(employeeService.findById(employee.getId())).thenReturn(employee);
        when(productService.findById(product.getId())).thenReturn(product);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(OrderInputDTO.class, new OrderInputDTOSerializer());
        mapper.registerModule(module);
        mvc.perform(put("/api/v2/orders/{id}", "id")
                .content(mapper.writeValueAsString(orderInputDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteOrder() throws Exception {
        doNothing().when(orderService).delete(order.getId());
        mvc.perform(delete("/api/v2/orders/{id}", order.getId())
                .header("login", "user"))
                .andExpect(status().isNoContent());
    }
}