package com.neoflex.study.shop.repository;

import com.neoflex.study.shop.ShopApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {ShopApplication.class})
public class CustomerRepositoryTest {
    @Autowired
    private CustomerRepository customerRepository;

    @Test
    @Transactional
    public void findByFullNameAndEmail() {
        Assert.assertEquals(customerRepository.findByFullNameAndEmail("Ivan Ivanov", "mail@mail.ru"),
                customerRepository
                        .findAll()
                        .stream()
                        .filter(it -> "Ivan Ivanov".equals(it.getFullName()) &&
                                "mail@mail.ru".equals(it.getEmail())).findFirst());
    }
}
