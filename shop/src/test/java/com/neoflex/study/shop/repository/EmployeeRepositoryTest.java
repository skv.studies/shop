package com.neoflex.study.shop.repository;

import com.neoflex.study.shop.ShopApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {ShopApplication.class})
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    @Transactional
    public void findByPosition() {
        Assert.assertEquals(employeeRepository.findByPosition("packer"),
                employeeRepository
                        .findAll()
                        .stream()
                        .filter(it -> "packer".equals(it.getPosition())).collect(Collectors.toList()));
    }
}
