package com.neoflex.study.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "EmployeeOutputDTO", description = "Employee's DTO for output")
public class EmployeeOutputDTO {
    @ApiModelProperty(value = "Employee's dto full name")
    private String fullName;

    @ApiModelProperty(value = "Employee's dto position")
    private String position;

    @ApiModelProperty(value = "Employee's dto orders")
    private List<OrderOutputDTO> ordersDTO;
}
