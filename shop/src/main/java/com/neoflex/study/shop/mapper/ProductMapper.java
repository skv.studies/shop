package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.ProductDTO;
import com.neoflex.study.shop.entity.Product;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface ProductMapper {
    ProductDTO productToProductDTO(Product product);

    @InheritInverseConfiguration
    Product productDTOToProduct(ProductDTO source);
}
