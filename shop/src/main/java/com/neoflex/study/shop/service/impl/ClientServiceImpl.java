package com.neoflex.study.shop.service.impl;

import com.neoflex.study.shop.controller.exception.ClientNotFoundException;
import com.neoflex.study.shop.entity.CheckClientRequest;
import com.neoflex.study.shop.entity.CheckClientResponse;
import com.neoflex.study.shop.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class ClientServiceImpl implements ClientService {
    private WebClient userClient;

    @Autowired
    public ClientServiceImpl() {
        this.userClient = WebClient.create("http://localhost:8081/api/v2/clients");
    }

    public Mono<CheckClientResponse> findByLogin(String login) {
        return userClient.post()
                .uri("/check")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(new CheckClientRequest(login))
                .retrieve()
                .bodyToMono(CheckClientResponse.class);
    }

    public void checkClient(String login) {
        findByLogin(login)
                .doOnNext(value -> {
                    if (!value.isExist()) {
                        throw new ClientNotFoundException(login);
                    }
                })
                .block();
    }
}