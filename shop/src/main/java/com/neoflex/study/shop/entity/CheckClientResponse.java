package com.neoflex.study.shop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckClientResponse {
    private boolean exist;

    public boolean isExist() {
        return exist;
    }
}