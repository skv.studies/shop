package com.neoflex.study.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "EmployeeInputDTO", description = "Employee's DTO for input")
public class EmployeeInputDTO {
    @NotNull
    @ApiModelProperty(value = "Employee's dto full name")
    private String fullName;

    @NotNull
    @ApiModelProperty(value = "Employee's dto position")
    private String position;
}

