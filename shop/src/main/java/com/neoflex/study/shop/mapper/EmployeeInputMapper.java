package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.EmployeeInputDTO;
import com.neoflex.study.shop.entity.Employee;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface EmployeeInputMapper {
    EmployeeInputDTO employeeToEmployeeInputDTO(Employee employee);

    @InheritInverseConfiguration
    Employee employeeInputDTOToEmployee(EmployeeInputDTO source);
}
