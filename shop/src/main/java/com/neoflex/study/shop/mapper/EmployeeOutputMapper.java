package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.EmployeeOutputDTO;
import com.neoflex.study.shop.entity.Employee;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface EmployeeOutputMapper {
    EmployeeOutputDTO employeeToEmployeeOutputDTO(Employee employee);

    @InheritInverseConfiguration
    Employee employeeOutputDTOToEmployee(EmployeeOutputDTO source);
}