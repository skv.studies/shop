package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Employee;

import java.util.List;

public interface EmployeeService {
    Employee findById(String id);

    List<Employee> findAll();

    String create(Employee employee);

    void update(String id, Employee employee);

    void delete(String id);
}
