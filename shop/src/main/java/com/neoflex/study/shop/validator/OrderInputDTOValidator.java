package com.neoflex.study.shop.validator;

import com.neoflex.study.shop.dto.OrderInputDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class OrderInputDTOValidator implements Validator {
    @Override
    public boolean supports(Class aClass) {
        return OrderInputDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        OrderInputDTO orderInputDTO = (OrderInputDTO) object;
        if (orderInputDTO.getCreateDate().after(orderInputDTO.getFinishDate())) {
            errors.reject("wrong time period");
        }
    }
}
