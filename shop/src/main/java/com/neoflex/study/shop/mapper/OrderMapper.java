package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.entity.Order;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface OrderMapper {
    OrderOutputDTO orderToOrderDTO(Order order);

    @InheritInverseConfiguration
    Order orderDTOToOrder(OrderOutputDTO source);
}