package com.neoflex.study.shop.controller;

import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.dto.OrderInputDTO;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.entity.Product;
import com.neoflex.study.shop.mapper.OrderInputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.service.*;
import com.neoflex.study.shop.validator.OrderInputDTOValidator;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/api/v2/orders")
@Api(value = "/api/v2/orders", description = "CRUD operations performed on orders")
public class OrderController {
    private final OrderService orderService;

    private final CustomerService customerService;

    private final EmployeeService employeeService;

    private final ProductService productService;

    private final OrderMapper orderMapper;

    private final OrderInputMapper orderInputMapper;

    private final ClientService clientService;

    @Autowired
    private OrderInputDTOValidator orderInputDTOValidator;

    @InitBinder
    public void initMerchantOnlyBinder(WebDataBinder binder) {
        binder.addValidators(orderInputDTOValidator);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get order's dto by id", response = OrderOutputDTO.class)
    @ApiResponses(@ApiResponse(code = 200, message = "Order's dto was found"))
    public ResponseEntity<OrderOutputDTO> getOrder(@RequestHeader String login, @ApiParam(value = "Order's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        OrderOutputDTO orderOutputDTO = orderMapper.orderToOrderDTO(orderService.findById(id));
        return ResponseEntity.ok(orderOutputDTO);
    }

    @GetMapping()
    @ApiOperation(value = "Get all orders's dto", response = OrderOutputDTO.class, responseContainer = "List")
    @ApiResponses(@ApiResponse(code = 200, message = "Orders' dto were found"))
    public ResponseEntity<List<OrderOutputDTO>> getAllOrders(@RequestHeader String login) {
        clientService.checkClient(login);
        List<OrderOutputDTO> orderOutputDTOS = orderService.findAll().stream()
                .map(order -> orderMapper.orderToOrderDTO(order)).collect(Collectors.toList());
        return ResponseEntity.ok(orderOutputDTOS);
    }

    @PostMapping()
    @ApiOperation(value = "Create order")
    @ApiResponses(@ApiResponse(code = 201, message = "Order was created"))
    public ResponseEntity<Response> createOrder(@RequestHeader String login, @RequestBody @Valid @ApiParam(value = "Order's dto", required = true)
            OrderInputDTO orderInputDTO) {
        clientService.checkClient(login);
        Order order = orderInputMapper.orderInputDTOToOrder(orderInputDTO);
        order.setCustomer(customerService.findById(orderInputDTO.getCustomerId()));
        order.setEmployee(employeeService.findById(orderInputDTO.getEmployeeId()));
        List<Product> products = new ArrayList<>();
        List<String> productsId = orderInputDTO.getProductId();
        for (String productId : productsId) {
            products.add(productService.findById(productId));
        }
        order.setProducts(products);
        return new ResponseEntity(orderService.create(order), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update order")
    @ApiResponses(@ApiResponse(code = 204, message = "Order was updated"))
    public ResponseEntity<Response> updateOrder(@RequestHeader String login, @ApiParam(value = "Order's id", required = true)
    @PathVariable String id,
                                                @RequestBody @Valid @ApiParam(value = "Order's dto", required = true)
                                                        OrderInputDTO orderInputDTO) {
        clientService.checkClient(login);
        Order order = orderInputMapper.orderInputDTOToOrder(orderInputDTO);
        order.setCustomer(customerService.findById(orderInputDTO.getCustomerId()));
        order.setEmployee(employeeService.findById(orderInputDTO.getEmployeeId()));
        List<Product> products = new ArrayList<>();
        List<String> productsId = orderInputDTO.getProductId();
        for (String productId : productsId) {
            products.add(productService.findById(productId));
        }
        order.setProducts(products);
        orderService.update(id, order);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete order")
    @ApiResponses(@ApiResponse(code = 204, message = "Order was deleted"))
    public ResponseEntity<Response> deleteOrder(@RequestHeader String login, @ApiParam(value = "Order's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        orderService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
