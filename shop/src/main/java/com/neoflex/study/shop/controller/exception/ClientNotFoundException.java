package com.neoflex.study.shop.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientNotFoundException extends ResponseStatusException {
    public ClientNotFoundException(String login) {
        super(HttpStatus.FORBIDDEN, String.format("Client with login %s not found.", login));
    }
}
