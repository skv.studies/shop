package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.CustomerOutputDTO;
import com.neoflex.study.shop.entity.Customer;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerOutputMapper {
    CustomerOutputDTO customerToCustomerOutputDTO(Customer customer);

    @InheritInverseConfiguration
    Customer customerOutputDTOToCustomer(CustomerOutputDTO customer);
}