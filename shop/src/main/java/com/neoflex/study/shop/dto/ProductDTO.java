package com.neoflex.study.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ProductDTO", description = "Product's DTO")
public class ProductDTO {
    @NotNull
    @ApiModelProperty(value = "Product's type")
    private String type;

    @NotNull
    @ApiModelProperty(value = "Product's name")
    private String name;

    @NotNull
    @ApiModelProperty(value = "Product's description")
    private String description;

    @NotNull
    @Min(0)
    @ApiModelProperty(value = "Product's price", example = "0")
    private Float price;
}
