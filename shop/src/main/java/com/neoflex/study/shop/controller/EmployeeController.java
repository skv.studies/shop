package com.neoflex.study.shop.controller;

import com.neoflex.study.shop.dto.EmployeeInputDTO;
import com.neoflex.study.shop.dto.EmployeeOutputDTO;
import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.entity.Employee;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.mapper.EmployeeInputMapper;
import com.neoflex.study.shop.mapper.EmployeeOutputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.service.EmployeeService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.neoflex.study.shop.service.ClientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/api/v2/employees")
@Api(value = "/api/v2/employees", description = "CRUD operations performed on employees")
public class EmployeeController {
    private final EmployeeService employeeService;

    private final EmployeeInputMapper employeeInputMapper;

    private final EmployeeOutputMapper employeeOutputMapper;

    private final OrderMapper orderMapper;

    private final ClientService clientService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Get employee's dto by id", response = EmployeeOutputDTO.class)
    @ApiResponses(@ApiResponse(code = 200, message = "Employee's dto was found"))
    public ResponseEntity<EmployeeOutputDTO> getEmployee(@RequestHeader String login, @ApiParam(value = "Employee's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        EmployeeOutputDTO employeeOutputDTO = employeeOutputMapper.employeeToEmployeeOutputDTO(employeeService.findById(id));
        List<OrderOutputDTO> orderOutputDTOS = employeeService.findById(id).getOrders().stream()
                .map(order -> orderMapper.orderToOrderDTO(order)).collect(Collectors.toList());
        employeeOutputDTO.setOrdersDTO(orderOutputDTOS);
        return ResponseEntity.ok(employeeOutputDTO);
    }

    @GetMapping()
    @ApiOperation(value = "Get all employees's dto", response = EmployeeOutputDTO.class, responseContainer = "List")
    @ApiResponses(@ApiResponse(code = 200, message = "Employees' dto were found"))
    public ResponseEntity<List<EmployeeOutputDTO>> getAllEmployees(@RequestHeader String login) {
        clientService.checkClient(login);
        List<Employee> employees = employeeService.findAll();
        List<EmployeeOutputDTO> employeeOutputDTOS = new ArrayList<>();
        for (Employee employee : employees) {
            EmployeeOutputDTO employeeOutputDTO = employeeOutputMapper.employeeToEmployeeOutputDTO(employee);
            List<Order> orders = employee.getOrders();
            List<OrderOutputDTO> orderOutputDTOS = new ArrayList<>();
            for (Order order : orders) {
                orderOutputDTOS.add(orderMapper.orderToOrderDTO(order));
            }
            employeeOutputDTO.setOrdersDTO(orderOutputDTOS);
            employeeOutputDTOS.add(employeeOutputDTO);
        }
        return ResponseEntity.ok(employeeOutputDTOS);
    }

    @PostMapping()
    @ApiOperation(value = "Create employee")
    @ApiResponses(@ApiResponse(code = 201, message = "Employee was created"))
    public ResponseEntity<Response> createEmployee(@RequestHeader String login, @RequestBody @Valid @ApiParam(value = "Employee's dto", required = true)
            EmployeeInputDTO employeeDTO) {
        clientService.checkClient(login);
        Employee employee = employeeInputMapper.employeeInputDTOToEmployee(employeeDTO);
        return new ResponseEntity(employeeService.create(employee), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update employee")
    @ApiResponses(@ApiResponse(code = 204, message = "Employee was updated"))
    public ResponseEntity<Response> updateEmployee(@RequestHeader String login, @ApiParam(value = "Employee's id", required = true)
    @PathVariable String id,
                                                   @RequestBody @Valid @ApiParam(value = "Employee's dto", required = true)
                                                           EmployeeInputDTO employeeDTO) {
        clientService.checkClient(login);
        Employee employee = employeeInputMapper.employeeInputDTOToEmployee(employeeDTO);
        employeeService.update(id, employee);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete employee")
    @ApiResponses(@ApiResponse(code = 204, message = "Employee was deleted"))
    public ResponseEntity<Response> deleteEmployee(@RequestHeader String login, @ApiParam(value = "Employee's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        employeeService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
