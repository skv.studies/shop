package com.neoflex.study.shop.dto;

import com.neoflex.study.shop.entity.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "OrderOutputDTO", description = "Order's DTO")
public class OrderOutputDTO {
    @ApiModelProperty(value = "Orders's dto create date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createDate;

    @ApiModelProperty(value = "Orders's dto finish date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @ApiModelProperty(value = "Order's status")
    private String status;

    @ApiModelProperty(value = "Order's customer")
    private String customerId;

    @ApiModelProperty(value = "Order's employee")
    private String employeeId;

    @ApiModelProperty(value = "Order's products")
    private List<Product> products;
}
