package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.OrderInputDTO;
import com.neoflex.study.shop.entity.Order;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface OrderInputMapper {
    OrderInputDTO orderToOrderInputDTO(Order order);

    @InheritInverseConfiguration
    Order orderInputDTOToOrder(OrderInputDTO source);
}
