package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.CheckClientResponse;
import reactor.core.publisher.Mono;

public interface ClientService {
    Mono<CheckClientResponse> findByLogin(String login);

    void checkClient(String login);
}