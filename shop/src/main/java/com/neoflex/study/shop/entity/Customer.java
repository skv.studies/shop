package com.neoflex.study.shop.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customers")
public class Customer {
    @Column(name = "id")
    private @Id
    String id = UUID.randomUUID().toString();

    @NonNull
    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @NonNull
    @Column(name = "gender")
    private String gender;

    @NonNull
    @Column(name = "full_Name")
    private String fullName;

    @NonNull
    @Column(name = "phone")
    private String phone;

    @NonNull
    @Column(name = "email")
    private String email;

    @NonNull
    @Column(name = "address")
    private String address;

    @NonNull
    @OneToMany(mappedBy = "customerId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Order> orders;
}
