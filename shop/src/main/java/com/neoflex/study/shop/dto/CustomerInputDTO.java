package com.neoflex.study.shop.dto;

import com.neoflex.study.shop.validator.PhoneValid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CustomerInputDTO", description = "Customer's DTO for input")
public class CustomerInputDTO {
    @NotNull
    @ApiModelProperty(value = "Customer's dto birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @NotNull
    @ApiModelProperty(value = "Customer's gender ")
    private String gender;

    @NotNull
    @ApiModelProperty(value = "Customer's dto full name")
    private String fullName;

    @NotNull
    @PhoneValid
    @ApiModelProperty(value = "Customer's dto phone")
    private String phone;

    @NotNull
    @Email
    @ApiModelProperty(value = "Customer's email")
    private String email;

    @NotNull
    @ApiModelProperty(value = "Customer's dto address")
    private String address;
}
