package com.neoflex.study.shop.controller;

import com.neoflex.study.shop.dto.CustomerInputDTO;
import com.neoflex.study.shop.dto.CustomerOutputDTO;
import com.neoflex.study.shop.dto.OrderOutputDTO;
import com.neoflex.study.shop.entity.Customer;
import com.neoflex.study.shop.entity.Order;
import com.neoflex.study.shop.mapper.CustomerInputMapper;
import com.neoflex.study.shop.mapper.CustomerOutputMapper;
import com.neoflex.study.shop.mapper.OrderMapper;
import com.neoflex.study.shop.service.ClientService;
import com.neoflex.study.shop.service.CustomerService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

@AllArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/api/v2/customers")
@Api(value = "/api/v2/customers", description = "CRUD operations performed on customers")
public class CustomerController {
    private final CustomerService customerService;

    private final ClientService clientService;

    private final CustomerInputMapper customerInputMapper;

    private final CustomerOutputMapper customerOutputMapper;

    private final OrderMapper orderMapper;

    @GetMapping("/{id}")
    @ApiOperation(value = "Get customer's dto by id", response = CustomerOutputDTO.class)
    @ApiResponses(@ApiResponse(code = 200, message = "Customer's dto was found"))
    public ResponseEntity<CustomerOutputDTO> getCustomer(@RequestHeader String login, @ApiParam(value = "Customer's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        CustomerOutputDTO customerOutputDTO = customerOutputMapper.customerToCustomerOutputDTO(customerService.findById(id));
        List<OrderOutputDTO> orderOutputDTOS = customerService.findById(id).getOrders().stream()
                .map(order -> orderMapper.orderToOrderDTO(order)).collect(Collectors.toList());
        customerOutputDTO.setOrdersDTO(orderOutputDTOS);
        return ResponseEntity.ok(customerOutputDTO);
    }

    @GetMapping()
    @ApiOperation(value = "Get all customers's dto", response = CustomerOutputDTO.class, responseContainer = "List")
    @ApiResponses(@ApiResponse(code = 200, message = "Customers' dto were found"))
    public ResponseEntity<List<CustomerOutputDTO>> getAllCustomers(@RequestHeader String login) {
        clientService.checkClient(login);
        List<Customer> customers = customerService.findAll();
        List<CustomerOutputDTO> customerOutputDTOS = new ArrayList<>();
        for (Customer customer : customers) {
            CustomerOutputDTO customerOutputDTO = customerOutputMapper.customerToCustomerOutputDTO(customer);
            List<Order> orders = customer.getOrders();
            List<OrderOutputDTO> orderOutputDTOS = new ArrayList<>();
            for (Order order : orders) {
                orderOutputDTOS.add(orderMapper.orderToOrderDTO(order));
            }
            customerOutputDTO.setOrdersDTO(orderOutputDTOS);
            customerOutputDTOS.add(customerOutputDTO);
        }
        return ResponseEntity.ok(customerOutputDTOS);
    }

    @PostMapping()
    @ApiOperation(value = "Create customer")
    @ApiResponses(@ApiResponse(code = 201, message = "Customer was created"))
    public ResponseEntity<Response> createCustomer(@RequestHeader String login, @RequestBody @Valid @ApiParam(value = "Customer's dto", required = true)
            CustomerInputDTO customerDTO) {
        clientService.checkClient(login);
        Customer customer = customerInputMapper.customerInputDTOToCustomer(customerDTO);
        return new ResponseEntity(customerService.create(customer), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update customer")
    @ApiResponses(@ApiResponse(code = 204, message = "Customer was updated"))
    public ResponseEntity<Response> updateCustomer(@RequestHeader String login, @ApiParam(value = "Customer's id", required = true)
    @PathVariable String id,
                                                   @RequestBody @Valid @ApiParam(value = "Customer's dto", required = true)
                                                           CustomerInputDTO customerDTO) {
        clientService.checkClient(login);
        Customer customer = customerInputMapper.customerInputDTOToCustomer(customerDTO);
        customerService.update(id, customer);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete customer")
    @ApiResponses(@ApiResponse(code = 204, message = "Customer was deleted"))
    public ResponseEntity<Response> deleteCustomer(@RequestHeader String login, @ApiParam(value = "Customer's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        customerService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
