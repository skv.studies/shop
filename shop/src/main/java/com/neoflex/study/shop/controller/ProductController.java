package com.neoflex.study.shop.controller;

import com.neoflex.study.shop.dto.ProductDTO;
import com.neoflex.study.shop.entity.Product;
import com.neoflex.study.shop.mapper.ProductMapper;
import com.neoflex.study.shop.service.ClientService;
import com.neoflex.study.shop.service.ProductService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/api/v2/products")
@Api(value = "/api/v2/products", description = "CRUD operations performed on products")
public class ProductController {
    private final ProductService productService;

    private final ProductMapper productMapper;

    private final ClientService clientService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Get product's dto by id", response = ProductDTO.class)
    @ApiResponses(@ApiResponse(code = 200, message = "Product's dto was found"))
    public ResponseEntity<ProductDTO> getProduct(@RequestHeader String login, @ApiParam(value = "Product's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        return ResponseEntity.ok(productMapper.productToProductDTO(productService.findById(id)));
    }

    @GetMapping()
    @ApiOperation(value = "Get all products's dto", response = ProductDTO.class, responseContainer = "List")
    @ApiResponses(@ApiResponse(code = 200, message = "Products' dto were found"))
    public ResponseEntity<List<ProductDTO>> getAllProducts(@RequestHeader String login) {
        clientService.checkClient(login);
        List<Product> products = productService.findAll();
        List<ProductDTO> dtos = new ArrayList<>();
        for (Product product : products) {
            dtos.add(productMapper.productToProductDTO(product));
        }
        return ResponseEntity.ok(dtos);
    }

    @PostMapping()
    @ApiOperation(value = "Create product")
    @ApiResponses(@ApiResponse(code = 201, message = "Product was created"))
    public ResponseEntity<Response> createProduct(@RequestHeader String login, @RequestBody @Valid @ApiParam(value = "Product's dto", required = true)
            ProductDTO productDTO) {
        clientService.checkClient(login);
        Product product = productMapper.productDTOToProduct(productDTO);
        return new ResponseEntity(productService.create(product), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update product")
    @ApiResponses(@ApiResponse(code = 204, message = "Product was updated"))
    public ResponseEntity<Response> updateProduct(@RequestHeader String login, @ApiParam(value = "Product's id", required = true)
    @PathVariable String id,
                                                  @RequestBody @Valid @ApiParam(value = "Product's dto", required = true)
                                                          ProductDTO productDTO) {
        clientService.checkClient(login);
        Product product = productMapper.productDTOToProduct(productDTO);
        productService.update(id, product);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete product")
    @ApiResponses(@ApiResponse(code = 204, message = "Product was deleted"))
    public ResponseEntity<Response> deleteProduct(@RequestHeader String login, @ApiParam(value = "Product's id", required = true)
    @PathVariable String id) {
        clientService.checkClient(login);
        productService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
