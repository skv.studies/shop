package com.neoflex.study.shop.service;

import com.neoflex.study.shop.entity.Customer;

import java.util.List;

public interface CustomerService {
    Customer findById(String id);

    List<Customer> findAll();

    String create(Customer customer);

    void update(String id, Customer customer);

    void delete(String id);
}
