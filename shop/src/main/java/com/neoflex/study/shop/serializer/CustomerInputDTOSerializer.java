package com.neoflex.study.shop.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.neoflex.study.shop.dto.CustomerInputDTO;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class CustomerInputDTOSerializer extends StdSerializer<CustomerInputDTO> {
    public CustomerInputDTOSerializer() {
        this(null);
    }

    public CustomerInputDTOSerializer(Class<CustomerInputDTO> t) {
        super(t);
    }

    @Override
    public void serialize(
            CustomerInputDTO value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("birthday", new SimpleDateFormat("yyyy-MM-dd").format(value.getBirthday()));
        jgen.writeStringField("gender", value.getGender());
        jgen.writeStringField("fullName", value.getFullName());
        jgen.writeStringField("phone", value.getPhone());
        jgen.writeStringField("email", value.getEmail());
        jgen.writeStringField("address", value.getAddress());
        jgen.writeEndObject();
    }
}
