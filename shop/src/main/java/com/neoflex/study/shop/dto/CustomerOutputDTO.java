package com.neoflex.study.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CustomerOutputDTO", description = "Customer's DTO for output")
public class CustomerOutputDTO {
    @ApiModelProperty(value = "Customer's dto birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @ApiModelProperty(value = "Customer's gender ")
    private String gender;

    @ApiModelProperty(value = "Customer's dto full name")
    private String fullName;

    @ApiModelProperty(value = "Customer's dto phone")
    private String phone;

    @ApiModelProperty(value = "Customer's email")
    private String email;

    @ApiModelProperty(value = "Customer's dto address")
    private String address;

    @ApiModelProperty(value = "Customer's dto orders")
    private List<OrderOutputDTO> ordersDTO;
}
