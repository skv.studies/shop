package com.neoflex.study.shop.mapper;

import com.neoflex.study.shop.dto.CustomerInputDTO;
import com.neoflex.study.shop.entity.Customer;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerInputMapper {
    CustomerInputDTO customerToCustomerInputDTO(Customer customer);

    @InheritInverseConfiguration
    Customer customerInputDTOToCustomer(CustomerInputDTO customer);
}