package com.neoflex.study.shop.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.neoflex.study.shop.dto.OrderInputDTO;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class OrderInputDTOSerializer extends StdSerializer<OrderInputDTO> {
    public OrderInputDTOSerializer() {
        this(null);
    }

    public OrderInputDTOSerializer(Class<OrderInputDTO> t) {
        super(t);
    }

    @Override
    public void serialize(
            OrderInputDTO value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("createDate", new SimpleDateFormat("yyyy-MM-dd").format(value.getCreateDate()));
        jgen.writeStringField("finishDate", new SimpleDateFormat("yyyy-MM-dd").format(value.getFinishDate()));
        jgen.writeStringField("status", value.getStatus());
        jgen.writeStringField("customerId", value.getCustomerId());
        jgen.writeStringField("employeeId", value.getEmployeeId());
        jgen.writeFieldName("productId");
        jgen.writeStartArray();
        for (String productId : value.getProductId()) {
            jgen.writeString(productId);
        }
        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}
